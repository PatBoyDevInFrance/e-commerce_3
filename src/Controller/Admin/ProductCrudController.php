<?php

namespace App\Controller\Admin;

use App\Entity\CartDetails;
use App\Entity\OrderDetails;
use App\Entity\Product;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\SlugField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\HttpFoundation\Request;

class ProductCrudController extends AbstractCrudController
{   
    

    public static function getEntityFqcn(): string
    {
        return Product::class;
    }
    

    public function stocks(CartDetails $cartDetails,OrderDetails $orderDetails, Product $product, Request $request): int 
    {   
        
       return $product->getQuantity()-1;
        // $stock = $product->getQuantity();

        // dd($cartDetails);

        //  foreach ($stock as  $stc) {
        //    $stc - $cartDetails->getQuantity();
        // }
    }

    
    public function configureFields(string $pageName ): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            TextField::new('name'),
            SlugField::new('slug')->setTargetFieldName('name')->hideOnIndex(),
            TextEditorField::new('description'),
            TextEditorField::new('moreInformation')->hideOnIndex(),
            MoneyField::new('price')->setCurrency('USD'),
            IntegerField::new('reduction'),
            IntegerField::new('quantity')->formatValue(function ($product, $orderDetails) { 
            //dd($orderDetails );
            //$qua = $orderDetails->getQuantity();
            //dd($qua);
            //return $qua  ;
            return $product < 3 ? sprintf('%d **FAIBLE STOCK**', $product) : $product;
            }),
            
            TextField::new('tags'),
            BooleanField::new('isBestSeller','Best Seller'),
            BooleanField::new('isNewArrival','New Arrival'),
            BooleanField::new('isFeatured','Featured'),
            BooleanField::new('isSpecialOffer','Special Offer'),
            AssociationField::new('category'),
            ImageField::new('image') ->setBasePath('assets/uploads/products/')
            ->setUploadDir('public/assets/uploads/products/')
            ->setUploadedFileNamePattern('[randomhash].[extension]')
            ->setRequired(false),
        ];
    }
    
}


// IntegerField::new('quantity')->formatValue(function ($product, $CartDetails) {
                
//     //return $product->getQuantity() < $CartDetails->getCartDetails() ? sprintf('%d **Faible STOCK**', $product) : $product;
//     // return $value < 10 ? sprintf('%d **Faible STOCK**', $value) : $value;
//     // return $CartDetails->getQuantity() ? $value : 'Coming soon...';
//    //dd($CartDetails->getQuantity());
// }),

// autre solution
// ->formatValue(function ($cartService, $product){
//     return $cartService->deleteAllToCart() - $product->getQuantity() ? sprintf('%d **Faible STOCK**', $product) : $product;
// })
