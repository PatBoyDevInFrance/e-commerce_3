<?php

namespace App\Controller\Cart;

use App\Entity\Cart;
use App\Entity\CartDetails;
use App\Entity\Product;
use App\Services\CartService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends AbstractController
{   
    private $cartService;

    public function __construct(CartService $cartService)
    {   
        $this->cartService = $cartService;
        
    }

    #[Route('/cart', name: 'cart')]
    public function index(): Response
    {       
        $cart = $this->cartService->getFullCart();
        if(!isset($cart['products'])){
            return $this->redirectToRoute('home');
        }

        //dd($cartService->getCart());

        return $this->render('cart/index.html.twig',[
            'cart' => $cart
        ]);
    }

    #[Route('/cart/add/{id}', name: 'cart_add')]
    public function addToCart($id, ): Response {
        //$cartService->deleteCart();
        $this->cartService->addToCart($id);

        //dd($cartService->getFullCart());       
        return $this->redirectToRoute('cart');
       
    }

    #[Route('/cart/delete/{id}', name: 'cart_delete')]
    public function deleteFromCart($id, ): Response {
        $this->cartService->deleteFromCart($id);
        //$cartService->addToCart($id);

        //dd($cartService->getFullCart());        
        
        return $this->redirectToRoute('cart');
    }

    #[Route('/cart/delete-all/{id}', name: 'cart_delete_all')]
    public function deleteAllCart($id, ): Response {
        $this->cartService->deleteAllToCart($id);
        //$cartService->addToCart($id);

        //dd($cartService->getFullCart());        
        
        return $this->redirectToRoute('cart');
    }


    // /**
    //  * @Route("/cart/add.json", name="add_cart_json", methods={"POST"})
    //  */
    // public function addToCartJson(Request $request, SessionInterface $session)
    // {
    //     $repositoryP = $this->getDoctrine()->getRepository(Product::class);
    //     $product = $repositoryP->find($request->request->get('product_id'));

    //     $objectManager = $this->getDoctrine()->getManager();

    //     if (!$product instanceof Product) {
    //         $status = 'ko';
    //         $message = 'Product not found';
    //     } else {
    //         // stock
    //         if ($product->getQuantity() < $request->request->get('quantity')) {
    //             $status = 'ko';
    //             $message = 'Missing quantity for product';
    //         } else {
    //             $cartId = $session->get('cart');

    //             if (!$cartId) {
    //                 $cart = new Cart();

    //                 $objectManager->persist($cart);
    //                 $objectManager->flush();

    //                 $session->set('cart', $cartId = $cart->getId());
    //             } else {
    //                 $repositoryCart = $this->getDoctrine()->getRepository(Cart::class);
    //                 /** @var Cart $cart */
    //                 $cart = $repositoryCart->find($cartId);
    //             }

    //             $cartProduct = new CartDetails();
    //             // $cartProduct->setCart($cart);
    //             // $cartProduct->setProduct($product);
    //             $cartProduct->setQuantity((int)$request->request->get('quantity'));


    //             $objectManager->persist($cartProduct);
    //             $objectManager->flush();

    //             $status = 'ok';
    //             $message = 'Added to cart';
    //         }
    //     }

    //     return new JsonResponse([
    //         'result' => $status,
    //         'message' => $message,
    //         'cart' => $cart,
    //     ]);
    // }

}
