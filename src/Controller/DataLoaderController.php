<?php

namespace App\Controller;

use App\Entity\Categories;
use App\Entity\OrderDetails;
use App\Entity\Product;
use App\Repository\UserRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DataLoaderController extends AbstractController
{
    #[Route('/data', name: 'data_loader')]
    public function index(EntityManagerInterface $em): Response
    {   
        $file_products = dirname(dirname(__DIR__))."/products.json";
        $file_categories = dirname(dirname(__DIR__))."/categories.json";
        $data_products = json_decode(file_get_contents($file_products))[0]->rows;
        $data_categories = json_decode(file_get_contents($file_categories))[0]->rows;
        $categories = [];

        foreach ($data_categories as $data_categorie ){
            $category = new Categories();
            $category->setName($data_categorie[1]);
            $category->setImage($data_categorie[3]);
            $em->persist($category);
            $categories[] = $category;

        }

        foreach ($data_products as $data_product){
            $product = new Product();
            // $order = new OrderDetails();
            // $stock = $product - (int)$order;
            // dd($stock);
            $product->setName($data_product[1])
                    ->setDescription($data_product[2])
                    ->setPrice($data_product[4])
                    ->setIsBestSeller($data_product[5])
                    ->setIsNewArrival($data_product[6])
                    ->setIsFeatured($data_product[7])
                    ->setIsSpecialOffer($data_product[8])
                    ->setImage($data_product[9])
                    ->setQuantity($data_product[10])
                    ->setTags($data_product[11])
                    ->setSlug($data_product[13])
                    ->setCreatedAt(new \DateTime());
                   $em->persist($product);
                   $products[] = $product;

        }
        //$user = $repoUser->find(1);
        //$user->setRoles(['ROLE_ADMIN']);
        $em->flush();

        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/DataLoaderController.php',
        ]);
    }
}
