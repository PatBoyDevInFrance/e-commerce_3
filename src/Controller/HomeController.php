<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\SearchProduct;
use App\Form\SearchProductType;
use App\Repository\HomeSliderRepository;
use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route('/', name: 'home')]
    public function index(ProductRepository $productRepo,
                            HomeSliderRepository $repoHomeSlider ): Response
    {   
        
        
          
        $products = $productRepo->findAll();
        $homeSlider = $repoHomeSlider->findBy(['isDisplayed'=> true]);
        //dd($homeSlider);
        
        $productBestSeller = $productRepo->findByIsBestSeller(1);
        $productSpecialOffer = $productRepo->findByIsSpecialOffer(1);
        $productNewArrival = $productRepo->findByIsNewArrival(1);
        $productFeatured = $productRepo->findByIsFeatured(1);

        //dd([$productBestSeller,$productSpecialOffer,$productNewArrival,$productFeatured]);
        return $this->render('home/index.html.twig',[
            'controller_name' => 'HomeController',
            'product' => $products,
            'productBestSeller' => $productBestSeller,
            'productSpecialOffer' => $productSpecialOffer,
            'productNewArrival' => $productNewArrival,
            'productFeatured' => $productFeatured,
            'homeSlider' => $homeSlider


        ]);
    }

    #[Route('/product/{slug}', name :'product_details')]
    public function show(?Product $product): Response {

        if(!$product){
            return $this->redirectToRoute("home");
        }
        
        return $this->render("home/single_product.html.twig",[
            'product' => $product

        ]);
    }

    #[Route('/shop', name: 'shop')]
    public function shop(ProductRepository $productRepo,Request $request): Response
                            
    {   
        
        $products = $productRepo->findAll();

        $search = new SearchProduct();
        $form = $this->createForm(SearchProductType::class, $search);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
        
             $products = $productRepo->findWithSearch($search);
                

        }

        return $this->render('home/shop.html.twig',[
            
            'products' => $products,
            'search' => $form->createView()
            
        ]);
    }
    
    
    
}
