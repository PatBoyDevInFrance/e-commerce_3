<?php

namespace App\Controller\Stripe;

use App\Entity\Order;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class StripeCancelPayementController extends AbstractController
{
    #[Route('/stripe-cancel-payement/{StripeCheckoutSessionId}', name: 'stripe_cancel_payement')]
    public function index(?Order $order): Response
    {   
        if( !$order || $order->getUser() !== $this->getUser()  ){
            return $this->redirectToRoute('home');
        }  

        return $this->render('stripe_cancel_payement/index.html.twig', [
            'order' => $order
        ]);
    }
}
