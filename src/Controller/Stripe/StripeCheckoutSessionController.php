<?php

namespace App\Controller\Stripe;

use App\Entity\Cart;
use App\Services\CartService;
use App\Services\OrderServices;
use Doctrine\ORM\EntityManagerInterface;
use Stripe\Checkout\Session;
use Stripe\Service\OrderService;
use Stripe\Stripe;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class StripeCheckoutSessionController extends AbstractController
{
    #[Route('/create-checkout-session/{reference}', name: 'create_checkout_session')]
    public function index(?Cart $cart, OrderServices $orderServices, EntityManagerInterface $em): Response
    {   
        $user = $this->getUser();
        if(!$cart){
            return $this->redirectToRoute('home');
        }
        
        
        $order = $orderServices->createOrder($cart);
        //$cart = $cartService->getFullCart();
        Stripe::setApiKey('sk_test_51IabWXA1Fknmzb3njfcRNz8XNuJnKXOA9RCtdAnopH1JVhcTiPVTii3THfEJykDLDBElr6vA1Z0teDFH8iISvzJc00NAXBUvGs');
        
        $checkout_session = Session::create([
            'customer_email' => $user->getEmail(),
            'payment_method_types' => ['card'],
            'line_items' => $orderServices->getLineItems($cart),
            'mode' => 'payment',
            'success_url' => $_ENV['YOUR_DOMAIN'].'/stripe-success-payement/{CHECKOUT_SESSION_ID}',
            'cancel_url' => $_ENV['YOUR_DOMAIN'].'/stripe-cancel-payement/{CHECKOUT_SESSION_ID}',

            ]);

            $order->setStripeCheckoutSessionId($checkout_session->id);
            $em->flush();


            
//echo json_encode(['id' => $checkout_session->id]);
        return $this->json(['id' => $checkout_session->id]);
    }
}
