<?php

namespace App\Controller\Stripe;

use App\Entity\Order;

use App\Services\CartService;
use App\Services\StockManagerService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class StripeSuccessPayementController extends AbstractController
{
    #[Route('stripe-success-payement/{StripeCheckoutSessionId}', name: 'stripe_success_payement')]
    public function index(?Order $order, CartService $cartService, EntityManagerInterface $em, StockManagerService $stockManager): Response
    {   
        if( !$order || $order->getUser() != $this->getUser()  ){
            return $this->redirectToRoute('home');
        }  
        if(!$order->getIsPaid()){
            // indiquer que la commande est payé
            $order->setIsPaid(true);
            // destockage
            $stockManager->deStock($order);
            
            $em->flush();
            $cartService->deleteCart();

            // un mail au client
        }
       // dd($order);
        return $this->render('stripe_success_payement/index.html.twig', [
            'order' => $order

        ]);
    }
}
