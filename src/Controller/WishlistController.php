<?php

namespace App\Controller;

use App\Entity\Cart;
use App\Services\CartService;
use App\Services\Coeur;
use MercurySeries\FlashyBundle\FlashyNotifier;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class WishlistController extends AbstractController
{
    #[Route('/wishlist', name: 'wishlist_index')]
    public function index(Coeur $coeur): Response
    {   
        
        return $this->render('wishlist/index.html.twig',[
            'coeurs' => $coeur,
            'items' => $coeur->getFullCart(),
        ]);
    }


    /**
     * @Route("/wishlist/add/{id}" , name="wishlist_add")
     */
    
    public function add($id, Coeur $coeur,  FlashyNotifier $flashy): Response
    {       
        $coeur->add($id);
        $flashy->success('coup de coeur ajouté','Your address has been saved');
        //$this->addFlash('address_message','Your address has been saved');

        return $this->redirectToRoute('home');
    }

    //#[Route('/product/{slug}', name :'product_details')]
    #[Route('/wishlist/delete/{id}', name: 'cart_delete')]
    public function deleteFromCart($id, Coeur $coeur ): Response {
        $coeur->remove($id);

        //$cartService->addToCart($id);

        //dd($cartService->deleteCart($id));        
        
        return $this->redirectToRoute('wishlist_index');
    }

    /**
     * @Route("/wishlist/decrease/{id}", name="decrease_to_cart")
     */
    public function decrease ($id, Coeur $coeur): Response
    {       
        $coeur->decrease($id);

        return $this->redirectToRoute('cart');
    }

   
}

    
