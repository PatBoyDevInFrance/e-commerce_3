<?php

namespace App\Entity;

class SearchProduct
{
    
    private $minPrice = null ;

    
    private $maxPrice = null ;
   
    /**
     * @var Categories[]
     */
    private $categorie = [];

    
    private $tags = null ;

    
    public function getMinPrice(): ?int
    {
        return $this->minPrice;
    }

    public function setMinPrice(?int $minPrice): self
    {
        $this->minPrice = $minPrice;

        return $this;
    }

    public function getMaxPrice(): ?int
    {
        return $this->maxPrice;
    }

    public function setMaxPrice(?int $maxPrice): self
    {
        $this->maxPrice = $maxPrice;

        return $this;
    }

    public function getcategorie(): ?array
    {
        return $this->categorie;
    }

    public function setcategorie(?array $categorie): self
    {
        $this->categorie = $categorie;

        return $this;
    }

    public function getTags(): ?string
    {
        return $this->tags;
    }

    public function setTags(?string $tags): self
    {
        $this->tags = $tags;

        return $this;
    }
}
