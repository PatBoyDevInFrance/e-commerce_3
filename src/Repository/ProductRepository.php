<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    /**
    * @return Product[] Returns an array of Product objects
    */
    public function findWithSearch($search)
    {
        $query = $this
                    ->createQueryBuilder('p');
           
        if($search->getMinPrice()){
            $query = $query->andWhere('p.price > '.$search->getMinPrice()*100);

        }
        if($search->getMaxPrice()){
            $query = $query->andWhere('p.price < '.$search->getMaxPrice()*100);
            
        }

        if($search->getTags()){
            $query = $query->andWhere('p.tags like :val')
                            ->setParameter('val',"%{$search->getTags()}%");
        }

        if ($search->getCategorie()){
            $query = $query->join('p.category','c')
                            ->andWhere('c.id IN (:categorie)')
                            ->setParameter('categorie',$search->getCategorie());

        }
        //dd($query->getQuery()->getResult());
        //dd($query->getQuery());
        return $query->getQuery()->getResult();
        
    }
    

    /*
    public function findOneBySomeField($value): ?Product
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
