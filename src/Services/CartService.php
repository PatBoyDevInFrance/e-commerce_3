<?php


namespace App\Services;

use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CartService {


    private $session;
    private $productRepo;
    private $tva = 0.2;


    public function __construct(SessionInterface $session, ProductRepository $productRepo )
    {
        $this->session = $session;
        $this->productRepo = $productRepo;
    }


    public function addToCart($id){

        $cart =  $this->getCart();
        if(isset($cart[$id])){

            $cart[$id]++;
        } else {
                $cart[$id] = 1;  
                }
        $this->updateCart($cart);
    }

    public function deleteFromCart($id){
        $cart =  $this->getCart();
        if(isset($cart[$id])) {
            //ptoduit déja dans le panier
            if ($cart[$id] > 1) {
                //produit exist plus d'une fois
                $cart[$id]--;
            } else{

            unset($cart[$id]);
            }

            $this->updateCart($cart);
        }
    }


    public function deleteAllToCart($id){
        $cart =  $this->getCart();
        if(isset($cart[$id])) {
            //ptoduit déja dans le panier   
            unset($cart[$id]);
            $this->updateCart($cart);
        }
        
    }


    public function deleteCart(){

        $this->updateCart([]);
    }


    public function updateCart($cart){

        $this->session->set('cart', $cart);
        $this->session->set('cartData', $this->getFullCart());
    }


    public function getCart(){

       return $this->session->get('cart',[]);
    }


    public function getFullCart(){
        $fullCart = [];
        $quantity_cart = 0;
        $subTotal = 0;
        $cart = $this->getCart();
        foreach( $cart as $id => $quantity) {
            $product = $this->productRepo->find($id);
            if($product) {
                //produit recupere avec succes
                    if($quantity > $product->getQuantity()){
                        $quantity = $product->getQuantity();
                        $cart[$id] = $quantity;
                        $this->updateCart($cart);
                    }
                $fullCart['products'][]=
                [
                    "quantity" => $quantity,
                    "product" => $product
                ];
                $quantity_cart += $quantity;
                $subTotal += $quantity * $product->getPrice()/100;
            } else {
                $this->deleteFromCart($id);

            }
        }
        $fullCart['data'] = [
            "quantity_cart" => $quantity_cart,
            "subTotalHT" => $subTotal,
            "Taxe" => round($subTotal*$this->tva,2),
            "subTotalTTC" => round(($subTotal + ($subTotal*$this->tva)), 2)
        ];


        return $fullCart;
    }


    
}