<?php

namespace App\Services;

use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
//test
class Coeur {
    protected $session;
    protected $productRepository;

    public function __construct(SessionInterface $session, ProductRepository $productRepository )
    {
        $this->session = $session;
        $this->productRepository = $productRepository;
    }

    public function add(int $id) {

        $panier = $this->session ->get('panier', []);

        if(!empty($panier[$id])){
            $panier[$id]++;
        } else {
            $panier[$id] = 1;
        }
       $this->session->set('panier', $panier);
      //$this->session->set('cartData', $this->getFullCart());
    }

    public function remove(int $id){
        $panier = $this->session->get('panier' , []);
        if(!empty($panier[$id])) {
            unset($panier[$id]);
        } else {
            $panier[$id] = 1 ;
        }

        $this->session->set('panier', $panier);
        
    }

    public function getFullCart() : array {
        $panier = $this->session->get('panier', []);
        $panierWithData = [];

        foreach( $panier as $id => $quantity){
            
            $panierWithData[] = [
                'product' => $this->productRepository->find($id),
                'quantity' => $quantity
            ];
        }
        return $panierWithData;
        
    }




    public function getTotal() : float 
    {
        $total = 0;
        

        foreach($this->getFullCart() as $item ){
            
            $total += $item['product']->getPrice() * $item['quantity'];
        }
        
        return $total;
        

    } 
    
    
    
    public function getQuantity() : float 
    {
        $totalQ = 0;
        

        foreach($this->getFullCart() as $item ){
            
            $totalQ += $item['quantity'];
        }
        $this->session->set('cartData', $this->getFullCart());
        return $totalQ;
        

    } 

    public function decrease(int $id){
        $panier = $this->session->get('panier', []); 
        if($panier[$id] > 1){
            $panier[$id]--;
        } else {
            unset($panier[$id]);

        }

        $this->session->set('panier', $panier);
    }
}